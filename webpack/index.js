var path = require('path');
var args = require('minimist')(process.argv.slice(2));
var merge = require('ramda').merge;
var cfgDefault = require('../config/default.js');

var options = merge({}, cfgDefault);

if (args.env) {
  try {
    var cfgEnv = require(path.resolve(process.env.PWD, args.env));
    options = merge(options, cfgEnv);
  } catch(e) {}
}

module.exports = options;
