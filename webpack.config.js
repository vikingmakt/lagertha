var path = require('path');
var pkg = require('./package.json');
var webpack = require('webpack');

var BannerPlugin = webpack.BannerPlugin;

var buildPath = path.resolve(__dirname, 'build');
var context = path.resolve(__dirname, 'src');

module.exports = {
  context: context,
  entry: './index.js',
  output: {
    path: buildPath,
    filename: 'lagertha.js',
    library: 'lagertha',
    libraryTarget: 'commonjs2',
    sourcePrefix: ''
  },
  resolve: {
    extensions: ['', '.js'],
    modulesDirectories: ['app', 'node_modules']
  },
  module: {
    loaders: [
      {test: /\.js$/, exclude: /node_modules/, loader: "babel", query: { presets: ['es2015'] }}
    ]
  },
  plugins: [
    new BannerPlugin('Lagertha v' + pkg.version + ' | (c) Vikign Makt | BSD-2-Clause License')
  ]
};
