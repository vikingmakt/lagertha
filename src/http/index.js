module.exports = {
  navigate: require('./navigate'),
  query: require('./query'),
  router: require('./router')
};
