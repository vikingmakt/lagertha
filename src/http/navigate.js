var router = require('./router');

module.exports = function navigate(url, state) {
  window.history.pushState(state, '', url);
  router.navigate(url);
};
