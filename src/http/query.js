var memoize = require('ramda').memoize;

function booleans(item) {
  item[1] = item[1] || true;
  return item;
}

function empty(item) {
  return !!item[0];
}

function split(item) {
  return item.split('=');
}

function toObject(all, item) {
  var attr = item[0].replace('[]', '');
  var value = item[1];

  if (all[attr]) {
    value = [value].concat(all[attr]);
  }

  all[attr] = value;
  return all;
}

var parse = memoize(function parse(query) {
  return query
    .replace('?', '')
    .split('&')
    .map(split)
    .filter(empty)
    .map(booleans)
    .reduce(toObject, {});
});

function get() {
  return parse(window.location.search);
}

module.exports = {
  get: get,
  parser: parse
};
