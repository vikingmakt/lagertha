var ee = require('../event/bus');
var events = require('./events');

ee.on(events.CONNECTION.CONNECT, createConnection);
ee.on(events.CONNECTION.DESTROY, destroyConnection);

var connection = false;

function createConnection(url) {
  if (connection && connection.readyState == WebSocket.OPEN) {
    return;
  }

  connection = new WebSocket(url);

  connection.onclose = function raidConnectionOnClose(event) {
    ee.trigger(events.CONNECTION.CLOSE, event.data);
    setTimeout.bind(null, function reconnect() {
      ee.trigger(events.CONNECTION.CONNECT, url);
    }, 2000);
  };

  connection.onmessage = function raidConnectionOnMessage(event) {
    ee.trigger(events.CONNECTION.MESSAGE, event.data);
  };

  connection.onopen = function raidConnectionOnOpen(event) {
    ee.trigger(events.CONNECTION.OPEN, event.target);
  };
}

function destroyConnection() {
  if (connection &&
      connection.readyState !== WebSocket.CLOSED &&
      connection.readyState !== WebSocket.CLOSING) {
    connection.onclose = function onClose(event) {
      ee.trigger(events.CONNECTION.CLOSE, event.data);
    };
    connection.close();
  }
}

function send(message) {
  return connection.send(message);
};

module.exports = {
  send: send
};
