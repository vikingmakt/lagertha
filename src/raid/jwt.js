var jwt = require('jsonwebtoken');

function encode(message, key) {
  return new Promise(function (resolve) {
    resolve(jwt.sign(message, key));
  });
}

function decode(message, key, future) {
  return new Promise(function (resolve) {
    resolve(jwt.verify(message, key));
  });
}

module.exports = {
  decode: decode,
  encode: encode
};
