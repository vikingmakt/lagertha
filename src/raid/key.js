var options = require('../options');

var MASTER_KEY = options.jwt.masterKey;
var KEY = MASTER_KEY;

function require(fn) {
  fn(KEY);
}

function reset() {
  KEY = MASTER_KEY;
}

function replace(key) {
  KEY = key;
}

module.exports = {
  require: require,
  reset: reset,
  replace: replace
};
