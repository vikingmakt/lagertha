require('./welcome');
var connection = require('./connection');
var ee = require('../event/bus');
var events = require('./events');
var jwt = require('./jwt');
var key = require('./key');
var uuid = require('../hash/uuid');

ee.on(events.CONNECTION.CLOSE, events.KEY.BROKEN);
ee.on(events.CONNECTION.MESSAGE, onConnectionMessage);
ee.on(events.MESSAGE.IN, onMessageIn);
ee.on(events.SEND, onSend);
ee.on(events.KEY.RECEIVED, onKeyReceived);
ee.on(events.KEY.BROKEN, key.reset);

var auth = false;

function onKeyReceived(message) {
  auth = true;
  key.replace(message.body.key);
}

function onConnectionMessage(message) {
  key.require(function (key) {
    jwt.decode(message, key).then(function (decoded) {
      ee.trigger(events.MESSAGE.IN, decoded);
    });
  });
}

function onMessageIn(message) {
  ee.trigger('action<'+message.header.action+'>', message);
  if (message.header.method) {
    ee.trigger('action<'+message.header.action+'>:method<'+message.header.method+'>', message);
  }
  if (message.header.etag) {
    ee.trigger(message.header.etag, message);
  }
};

function onSend(message) {
  key.require(function (key) {
    jwt.encode(message, key).then(function (encoded) {
      ee.trigger(events.MESSAGE.OUT, message);
      return connection.send(encoded);
    }).catch(function (err) {
      ee.one(events.KEY.RECEIVED, function () {
        ee.trigger(events.SEND, message);
      });
    });
  });
}

function close() {
  ee.trigger(events.CONNECTION.DESTROY);
}

function connect(url) {
  ee.trigger(events.CONNECTION.CONNECT, url);
}

function send(message, resolve) {
  if (auth) {
    return ee.trigger(events.SEND, message);
  }

  return ee.one(events.KEY.RECEIVED, function () {
    ee.trigger(events.SEND, message);
  });
}

module.exports = {
  close: close,
  connect: connect,
  send: send,
  events: events
};
