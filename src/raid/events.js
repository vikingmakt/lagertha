module.exports = {
  CONNECTION: {
    CLOSE: 'lagertha:raid:connection:close',
    CONNECT: 'lagertha:raid:connection:connect',
    DESTROY: 'lagertha:raid:connection:destroy',
    MESSAGE: 'lagertha:raid:connection:message',
    OPEN: 'lagertha:raid:connection:open'
  },
  KEY: {
    BROKEN: 'lagertha:raid:key:broken',
    RECEIVED: 'lagertha:raid:key:received'
  },
  MESSAGE: {
    IN: 'lagertha:raid:incoming_message',
    OUT: 'lagertha:raid:outgoing_message'
  },
  SEND: 'lagertha:raid:send'
};
