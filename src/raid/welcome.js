var ee = require('../event/bus');
var events = require('./events');
var uuid = require('../hash/uuid');
ee.on('action<welcome>:method<who>', onWelcomeWho);
ee.on('action<welcome>:method<take>', events.KEY.RECEIVED);

function onWelcomeWho() {
  var message = {
    header: {
      action: 'welcome',
      method: 'iam'
    },
    body: {
      name: "webclient-"+ uuid(),
      datetime: Date.now()
    }
  };

  ee.trigger(events.SEND, message);
}
