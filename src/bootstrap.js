var navigate = require('./http/navigate');
var router   = require('./http/router');

module.exports = function bootstrap() {
  router.navigate(window.location.pathname);

  document.addEventListener('click', function _globalDocumentClick(event) {
    if (event.target.nodeName == 'A' && !event.target.getAttribute('target')) {
      event.preventDefault();
      navigate(event.target.getAttribute('href'));
    }
  });

  window.onpopstate = function _windowOnpopstate() {
    router.navigate(window.location.pathname);
  };
};
