var KO = require('./key_owner');

function KeyOwnerValue(IO) {
  IO = KO(IO);
  return function CheckKeyOwnerValue(message) {
    if (message.body && !message.body.v) {
      throw new Error('DHTDB: Missing required key: `v` in params');
    }
    return IO(message);
  };
}

module.exports = KeyOwnerValue;
