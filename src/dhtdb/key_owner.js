function KeyOwner(IO) {
  return function CheckKeyOwner(message) {
    if (message.body && !message.body.k) {
      throw new Error('DHTDB: Missing required key: `k` in params');
    }

    if (message.body && !message.body.o) {
      throw new Error('DHTDB: Missing required key: `o` in params');
    }

    return IO(message);
  };
};

module.exports = KeyOwner;
