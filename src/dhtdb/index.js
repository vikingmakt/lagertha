var Action  = require('../message/action');
var Etag    = require('../message/etag');
var KO      = require('./key_owner');
var KOV     = require('./key_owner_value');
var Message = require('../message/message');
var Method  = require('../message/method');
var raid    = require('../raid');

var DHTDB  = Action(raid.send, 'dhtdb');
var Delete = KO(Etag(Method(DHTDB, 'del')));
var Get    = KO(Etag(Method(DHTDB, 'get')));
var Set    = KOV(Etag(Method(DHTDB, 'set')));

function operation(op) {
  return function doOperation(params) {
    return op(Message({
      body: params
    })).then(function (response) {
      return response.body;
    });
  };
}

module.exports = {
  delete: operation(Delete),
  get: operation(Get),
  set: operation(Set)
};
