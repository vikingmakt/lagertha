function WebSocketStream(Listener, url) {
  var connection = new WebSocket(url);

  connection.onopen = Listener.onopen;
  connection.onclose = Listener.onclose;
  connection.onmessage = Listener.onmessage;

  return {
    close: function WebSocketStreamClose() {
      connection.close();
    },
    send: function WebSocketStreamSend(data) {
      connection.send(data);
    }
  };
};

module.exports = WebSocketStream;
