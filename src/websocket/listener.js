function WebSocketListener(listener) {
  return Object.assign({
    onclose: function WebSocketListenerOnClose() {},
    onmessage: function WebSocketListenerOnMessage() {},
    onopen: function WebSocketListenerOnOpen() {}
  }, listener);
};

module.exports = WebSocketListener;
