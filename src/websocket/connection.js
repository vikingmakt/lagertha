var WebSocketStream = require('./stream');

function WebSocketConnection(Protocol, url) {
  var stream, listener = {};

  listener.onclose = function WebSocketConnectionListenerOnClose(event) {
    stream = WebSocketStream(listener, url);
    return Protocol.onclose(event);
  };
  listener.onmessage = function WebSocketConnectionListenerOnMessage(event) {
    return Protocol.onmessage(event.data);
  };
  listener.onopen = Protocol.onopen;

  stream = WebSocketStream(listener, url);

  return {
    close: function WebSocketConnectionClose() {
      stream.close();
    },
    send: function WebSocketConnectionSend(data) {
      stream.send(data);
    }
  };
};

module.exports = WebSocketConnection;
