function Message(extend) {
  return Object.assign({
    header: {},
    body: {}
  }, extend);
};

module.exports = Message;
