var ee   = require('../event/bus');
var uuid = require('../hash/uuid');

function Etag(IO) {
  return function MessageEtag(message) {
    return new Promise(function MessageEtagPromise(resolve) {
      message.header.etag = uuid();
      ee.one(message.header.etag, resolve);
      IO(message);
    });
  };
};

module.exports = Etag;
