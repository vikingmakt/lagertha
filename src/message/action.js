function Action(IO, action) {
  return function MessageAction(message) {
    message.header.action = action;
    return IO(message);
  };
}

module.exports = Action;
