function Sysdate(IO) {
  return function MessageSysdate(message) {
    message.header.__sysdate__ = Date.now() / 1000;
    IO(message);
  };
}

module.exports = Sysdate;
