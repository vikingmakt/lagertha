function Method(Action, method) {
  return function MessageMethod(message) {
    message.header.method = method;
    return Action(message);
  };
};

module.exports = Method;
