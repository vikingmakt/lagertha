var options = require('../../config/default');
if (typeof __LAGERTHA_OPTIONS__ !== 'undefined') {
  options = __LAGERTHA_OPTIONS__;
}

module.exports = Object.freeze(options);
