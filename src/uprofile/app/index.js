var Message  = require('../../message/message');
var Uprofile = require('../uprofile');


var App = Uprofile.actions('app');

function auth(name, token) {
  if (!name || !token) {
    throw new Error('Uprofile.app.auth(): name and token are required');
  }

  return App.auth(Message({
    body: {
      name: name,
      token: token
    }
  }));
}

function create(name) {
  if (!name) {
    throw new Error('Uprofile.app.create(): name is required');
  }

  return App.create(Message({
    body: { name: name }
  }));
}

function generateToken(name) {
  if (!name) {
    throw new Error('Uprofile.app.generateToken(): name is required');
  }

  return App.generateToken(Message({
    body: { name: name }
  }));
}

module.exports = {
  auth: auth,
  create: create,
  generateToken: generateToken,
  AUTH_OK: '8155b27e810443fb8aff242ea7a6ea00',
  AUTH_NOK: '74146d784d4d45c5bb39f514037b694a'
};
