var app      = require('./app');
var uprofile = require('./uprofile');
var user     = require('./user');

module.exports = {
  app: app,
  uprofile: uprofile,
  user: user
};
