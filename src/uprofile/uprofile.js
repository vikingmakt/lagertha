var Action = require('../message/action');
var Etag   = require('../message/etag');
var IO     = require('../raid').send;
var Method = require('../message/method');

var Uprofile = Action(IO, 'uprofile');

function actions(name) {
  var Auth     = Etag(Method(Uprofile, name + '_auth'));
  var Create   = Etag(Method(Uprofile, name + '_create'));
  var GenToken = Etag(Method(Uprofile, name + '_gen_token'));

  return {
    auth: Auth,
    create: Create,
    generateToken: GenToken
  };
};

module.exports = {
  Uprofile: Uprofile,
  actions: actions
};
