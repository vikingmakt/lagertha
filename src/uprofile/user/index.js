var Message  = require('../../message/message');
var Uprofile = require('../uprofile');


var User = Uprofile.actions('user');

function auth(user, type, client_token, user_token) {
  if (!user || !type || !client_token || !user_token) {
    throw new Error('Uprofile.user.auth(): user, type, client_token and user_token are required');
  }

  return User.auth(Message({
    body: {
      client_token: client_token,
      type: type,
      user: user,
      user_token: user_token
    }
  }));
}

function create(user, type) {
  if (!user || !type) {
    throw new Error('Uprofile.user.create(): user and type are required');
  }

  return User.create(Message({
    body: {
      user: user,
      type: type
    }
  }));
}

function generateToken(user, type) {
  if (!user || !type) {
    throw new Error('Uprofile.user.generateToken(): user and type are required');
  }

  return User.generateToken(Message({
    body: {
      user: user,
      type: type
    }
  }));
}

module.exports = {
  auth: auth,
  create: create,
  generateToken: generateToken,
  AUTH_OK: 'c922b91d0a0c41be81fbb91032d199a0',
  AUTH_NOK: 'd1fa69f405654b78b97b5eb1bf6b705b',
  TYPE_EMAIL: 'email'
};
