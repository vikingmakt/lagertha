var ee       = require('../event/bus');
var events   = require('./events');
var uprofile = require('../uprofile/user');

function byEmail(email) {
  var clientToken;

  ee.trigger(events.AUTH.START);

  uprofile.generateToken(email, uprofile.TYPE_EMAIL)
    .then(function requestTokenResponse(response) {
      clientToken = response.body.client_token;
      ee.trigger(events.TOKEN.RECEIVED, {
        email: email,
        token: response.body.user
      });
    });

  return function authByToken(userToken) {
    if (!clientToken) {
      ee.trigger(events.TOKEN.MISSING_OR_NOT_RECEIVED);
    }

    uprofile.auth(email, uprofile.TYPE_EMAIL, clientToken, userToken)
      .then(function authByTokenResponse(response) {
        if (response.header.code !== uprofile.AUTH_OK) {
          ee.trigger(events.AUTH.ERROR);
          return;
        }
        ee.trigger(events.AUTH.SUCCESS, response.body);
      });
  };
}

module.exports = byEmail;
