var ee      = require('../event/bus');
var events  = require('./events');
var Message = require('../message/message');
var session = require('./session');

function bySession(code) {
  ee.trigger(events.AUTH.START);

  session.auth(Message({
    body: code
  })).then(function SessionAuthResponse(response) {
    if (response.header.code !== session.AUTH_OK) {
      ee.trigger(events.AUTH.ERROR);
      return;
    }

    ee.trigger(events.AUTH.SUCCESS, response.body);
    return;
  });
};

module.exports = bySession;
