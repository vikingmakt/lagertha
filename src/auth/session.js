var Message = require('../message/message');
var Action  = require('../message/action');
var Etag    = require('../message/etag');
var Method  = require('../message/method');
var IO      = require('../raid').send;


var Session        = Action(IO, 'session');
var SessionAuth    = Etag(Method(Session, 'auth'));
var SessionDestroy = Etag(Method(Session, 'destroy'));

module.exports = {
  auth: SessionAuth,
  destroy: SessionDestroy,
  AUTH_OK: '8155b27e810443fb8aff242ea7a6ea00',
  AUTH_NOK: '8155b27e810443fb8aff242ea7a6ea01'
};
