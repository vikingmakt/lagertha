var ee      = require('../event/bus');
var events  = require('./events');
var session = require('./session');
var Message = require('../message/message');


function logout() {
  session.destroy(Message()).then(function logoutResponse(response) {
    ee.trigger(events.AUTH.LOGOUT);
  });
}

module.exports = logout;
