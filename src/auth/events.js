module.exports = {
  AUTH: {
    ERROR: 'lagertha:auth:error',
    LOGOUT: 'lagertha:auth:logout',
    START: 'lagertha:auth:start',
    SUCCESS: 'lagerht:auth:success'
  },
  TOKEN: {
    RECEIVED: 'lagertha:auth:token:received',
    MISSING_OR_NOT_RECEIVED: 'lagertha:auth:token:missing_or_not_received'
  }
};
