var nextTick = require('../next');

var events = {};

function eventListeners(event) {
  events[event] = events[event] || [];
  return events[event];
}

function off(event, handler) {
  if (events[event]) {
    events[event] = events[event].filter( function (listener) {
      return listener.future !== handler;
    });
  }
}

function on(event, future, one) {
  one = one || false;
  eventListeners(event).push({ one: one, future: future });
}

function one(event, future) {
  on(event, future, true);
}

function trigger() {
  var args = Array.prototype.slice.call(arguments);
  var event = args.shift();
  nextTick(function () {
    events[event] = eventListeners(event).map(function (listener) {
      if (typeof listener.future == 'string') {
        var _args = [listener.future].concat(args);
        trigger.apply(null, _args);
      } else {
        listener.future.apply(listener.future, args);
      }
      return listener;
    }).filter(function (listener) { return !listener.one; });
  });
}

module.exports = {
  off: off,
  on: on,
  one: one,
  trigger: trigger
};
