function storage(io) {
  return function storageVariable(name) {
    var cache;

    function get() {
      if (cache) { return cache; }
      return io.getItem(name);
    }

    function has() {
      return !!this.get();
    }

    function remove() {
      io.removeItem(name);
      cache = null;
    }

    function set(value) {
      io.setItem(name, value);
      cache = value;
    }

    return {
      get: get,
      has: has,
      remove: remove,
      set: set
    };
  };
}

module.exports = storage;
