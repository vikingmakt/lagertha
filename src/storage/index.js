var storage = require('./storage');

var local = storage(window.localStorage);
var session = storage(window.sessionStorage);

module.exports = {
  local: local,
  session: session
};
