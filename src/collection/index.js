var CollectionCursor = require('./cursor');
var m = require('../message');

function Collection(IO, name) {
  name = name.split(':');

  if (name.length !== 2) {
    throw new Error('Collection(): Invalid collection name, use `action:method`');
  }

  var Action = m.Action(IO, name[0]);
  var Cursor = CollectionCursor(Action, name[1]);
  var Find   = m.Etag(m.Method(Action, name[1] + '_find'));
  var Get    = m.Etag(m.Method(Action, name[1] + '_get'));

  function find(filters) {
    var message = m.Message();

    if (filters) {
      message.body = { filters: filters };
    }

    return Find(message).then(function (response) {
      response.body.cursor = Cursor(response.body.cursor);
      return response;
    });
  }

  function get(id) {
    if (!id) {
      throw new Error('Collection.get(): id is required');
    }

    return Get(m.Message({
      body: { _id: id }
    }));
  }

  return {
    find: find,
    get: get
  };
}

module.exports = Collection;
