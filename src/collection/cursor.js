var Etag = require('../message/etag');
var Message = require('../message/message');
var Method = require('../message/method');

function Cursor(Action, method) {
  var Close = Etag(Method(Action, method + '_cursor_close'));
  var Fetch = Etag(Method(Action, method + '_fetch'));

  return function CursorCreate(cursor) {
    var message = Message({
      body: {
        cursor: cursor
      }
    });

    function CursorClose() {
      return Close(message);
    }

    function CursorFetch() {
      return Fetch(message);
    };

    return {
      close: CursorClose,
      fetch: CursorFetch
    };
  };
}

module.exports = Cursor;
